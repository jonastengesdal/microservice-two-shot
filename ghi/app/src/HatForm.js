import React, {useEffect, useState} from 'react'


function HatForm(props) {

    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState("");
    const [locations, setLocations] = useState("");


    const handleChangeFabric = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleChangeStyleName = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleChangeColor= (event) => {
        const value = event.target.value;
        setColor(value);
    }


    const handleChangePicture= (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }


    const handleChangeLocation= (event) => {
        const value = event.target.value;
        setLocations(value);
    }


    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.fabric = fabric;
      data.style_name = styleName;
      data.color = color;
      data.picture_url = picture_url;
      data.locations = locations;


      const hatUrl = 'http://localhost:8090/api/hats/';
      const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const hatsResponse = await fetch(shoeUrl, fetchOptions);
      console.log(shoesResponse)
      if (hatsResponse.ok) {
        const newHat = await hatsResponse.json();
        setFabric("");
        setStyleName("");
        setColor("");
        setPictureUrl("");
        setLocations("");
      }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeFabric} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                        <label htmlFor="fabric">Fabric</label>
                        </div>

                        <div className="form-floating mb-3">
                        <input onChange={handleChangeStyleName}  placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                        <label htmlFor="style_name">Style Name</label>
                        </div>

                        <div className="form-floating mb-3">
                        <input onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                        <input onChange={handleChangePicture} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture Url</label>
                        </div>


                        <div className="mb-3">
                        <select onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                            <option  value="">Choose a location</option>
                            {props.locations.map(location => {
                                return (
                                    <option key={location.id} value={location.href}>
                                    {location.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
         </div>
    )
}


export default ShoeForm;
