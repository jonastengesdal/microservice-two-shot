import React from "react";

export default function HatList({hats,fetchHats}) {
    async function deleteHats(id) {
        const url = "http://localhost:8090/api/hats/${id}";
        const fetchConfig = {
            method: "DELETE",
        }
        const response = await fetch(url, fetchConfig);
        return fetchShoes(response);

    }

  return (
    <table className="table table-striped">
        <thead>
            <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Location</th>
            <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            {hats.map(hat => {
                return (
                    <tr key={hat.id}>
                        <td>{hat.fabric}</td>
                        <td>{hat.style_name}</td>
                        <td>{hat.color}</td>
                        <img
                            src={hat.picture_url}
                            alt=""
                            width="75px"
                            height="76px" />
                        <td>{hat.location}</td>
                        <td>
                            <button className="btn btn-danger" type="button" value={hat.id} onClick={() =>deleteHats(hat.id)}>Delete</button>
                        </td>

                    </tr>
                    );
                })}
        </tbody>
    </table>
  );
}
