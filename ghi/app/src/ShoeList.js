import React from "react";

export default function ShoesList({shoes,fetchShoes}) {
    async function deleteShoes(id) {
        const url = "http://localhost:8080/api/shoes/${id}";
        const fetchConfig = {
            method: "DELETE",
        }
        const response = await fetch(url, fetchConfig);
        return fetchShoes(response);

    }

  return (
    <table className="table table-striped">
        <thead>
            <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
            <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            {shoes.map(shoe => {
                return (
                    <tr key={shoe.id}>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.color}</td>
                        <img
                            src={shoe.picture_url}
                            alt=""
                            width="75px"
                            height="76px" />
                        <td>{shoe.bin}</td>
                        <td>
                            <button className="btn btn-danger" type="button" value={shoe.id} onClick={() =>deleteShoes(shoe.id)}>Delete</button>
                        </td>

                    </tr>
                    );
                })}
        </tbody>
    </table>
  );
}
