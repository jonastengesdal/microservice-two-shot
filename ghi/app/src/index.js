import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import reportWebVitals from './reportWebVitals';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

reportWebVitals();


async function loadShoesHats() {
  const shoesResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');
  if (shoesResponse.ok && hatsResponse.ok) {
    const hatsData = await hatsResponse.json();
    const shoesData = await shoesResponse.json();
    console.log(data);
    root.render(
      <React.StrictMode>
        {/* Set variable for App.js */}
        <App shoes={shoesData.shoes} hats={hatsData.hats} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadShoesHats();
