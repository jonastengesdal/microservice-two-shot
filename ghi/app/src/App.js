import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatList from './HatForm';
import Nav from './Nav';
import React, { useEffect, useState} from 'react';
import ShoeList from './ShoeList';
// import ShoeForm from './ShoeForm';

function App(props) {
  if (props.shoes === undefinedprops || props.hats === undefined) {
    return null;
  }

  return (

    <>
      <Nav />
      <div className="container">
        <Routes></Routes>
        {/* <ShoeForm /> */}

        {/* <LocationForm /> */}
        <ShoeList shoes={props.shoes} />
        {/* <HatForm} /> */}

        {/* <LocationForm /> */}
        <HatList hats={props.hats} />
      </div>
      </>
  );
}

export default App;
