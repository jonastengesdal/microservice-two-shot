import React, {useEffect, useState} from 'react'


function ShoeForm(props) {

    // <th>Manufucturer</th>
    // <th>Model Name</th>
    // <th>Color</th>
    // <th>Picture</th>
    // <th>Bin</th>

    const [manufacturer, setmanufacturer] = useState('');
    const [modelName, setmodelName] = useState('');
    const [color, setcolor] = useState('');
    const [picture_url, setpicture_url] = useState("");
    const [bins, setbin] = useState("");


    const handleChangeManufucturer = (event) => {
        const value = event.target.value;
        setmanufacturer(value);
    }

    const handleChangeModelName = (event) => {
        const value = event.target.value;
        setmodelName(value);
    }

    const handleChangeColor= (event) => {
        const value = event.target.value;
        setcolor(value);
    }


    const handleChangePicture= (event) => {
        const value = event.target.value;
        setpicture_url(value);
    }


    const handleChangeBin= (event) => {
        const value = event.target.value;
        setbin(value);
    }


    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.manufacturer = manufacturer;
      data.model_name = modelName;
      data.color = color;
      data.picture_url = picture_url;
      data.bin = bins;


      const shoeUrl = 'http://localhost:8080/api/shoes/';
      const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const shoesResponse = await fetch(shoeUrl, fetchOptions);
      console.log(shoesResponse)
      if (shoesResponse.ok) {
        const newShoe = await shoesResponse.json();
        setmanufacturer("");
        setmodelName("");
        setcolor("");
        setpicture_url("");
        setbin("");
      }
    }

    // const fetchData = async () => {
    //     const binUrl = "http://localhost:8100/api/bins/";
    //     const binResponse = await fetch(binUrl);
    //     if (binResponse.ok){
    //         const data = await binResponse.json();
    //         setbin(data.bins);
    //     }
    // }

    // useEffect(() => {fetchData();}, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeManufucturer} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                        </div>

                        <div className="form-floating mb-3">
                        <input onChange={handleChangeModelName}  placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                        <label htmlFor="model_name">Model Name</label>
                        </div>

                        <div className="form-floating mb-3">
                        <input onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                        <input onChange={handleChangePicture} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture Url</label>
                        </div>


                        <div className="mb-3">
                        <select onChange={handleChangeBin} required name="bin" id="bin" className="form-select">
                            <option  value="">Choose a bin</option>
                            {props.bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.href}>
                                    {bin.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
         </div>
    )
}


export default ShoeForm;
