from django.shortcuts import render
from .models import Hat, LocationVO
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href", "section_number", "shelf_number"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "fabric", "color", "picture_url", "id", "location"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        # try:
        location_href = content["location"]
        print(location_href)
        location = LocationVO.objects.get(id=location_href)
        content["location"] = location
        # except LocationVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid location id"},
        #         status=400,
        #     )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


# def api_show_hats(request, pk):
#     hat = Hat.objects.get(id=pk)
#     return JsonResponse(
#         hat,
#         encoder=HatDetailEncoder,
#         safe=False,
#     )
